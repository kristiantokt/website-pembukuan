from django.urls import path
from . import views

urlpatterns = [
    path('api/pembukuan', views.PembukuanListCreate.as_view()),
]