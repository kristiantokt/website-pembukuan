from pembukuan.models import Pembukuan
from pembukuan.serializers import PembukuanSerializer
from rest_framework import generics

# Create your views here.
class PembukuanListCreate(generics.ListCreateAPIView):
    queryset = Pembukuan.objects.all()
    serializer_class = PembukuanSerializer
