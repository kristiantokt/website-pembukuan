from django.db import models

# Create your models here.
class Pembukuan(models.Model):
    pembukuan_id = models.AutoField(primary_key=True)
    pembukuan_title = models.CharField(max_length=100)
    pembukuan_type = models.IntegerField()
    pembukuan_detail = models.CharField(max_length=500)
    created_at = models.DateTimeField(auto_now_add=True)
    