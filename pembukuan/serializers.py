from rest_framework import serializers
from pembukuan.models import Pembukuan

class PembukuanSerializer(serializers.ModelSerializer):
    class Meta:
        model = Pembukuan
        fields = '__all__'